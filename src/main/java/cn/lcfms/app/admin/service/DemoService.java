package cn.lcfms.app.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.lcfms.app.admin.bean.DemoBean;
import cn.lcfms.app.admin.service.Dao.DemoDao;
import cn.lcfms.utils.Vardump;

@Service
public class DemoService {
	@Autowired
	private DemoDao dao;
	public void getDemo(){
		DemoBean demo = dao.getDemo(1);
		Vardump.print(demo);
	}
}
