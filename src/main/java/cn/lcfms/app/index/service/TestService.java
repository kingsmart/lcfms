package cn.lcfms.app.index.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.lcfms.app.index.service.dao.TestDao;
import cn.lcfms.utils.Vardump;

@Service
public class TestService {
	@Autowired
	TestDao dao;
	public void getList(){
		List<?> list = dao.getList();
		Vardump.print(list);
	}
}
