package cn.lcfms.bin;

import java.util.HashMap;
import java.util.List;

//综合缓存类
public class BaseCache {
	//后台栏目缓存
    public static List<HashMap<String, Object>> itemcache;
    //前台栏目缓存
    public static List<HashMap<String, Object>> catelist; 
    //debug模式下判断文件是否修改
    public static List<HashMap<String, Long>> fileCache;
}
