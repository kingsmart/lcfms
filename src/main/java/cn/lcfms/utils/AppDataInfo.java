package cn.lcfms.utils;

import java.io.Serializable;

/**
 * 接口数据泛型类
 * @author Administrator
 *
 */
public class AppDataInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	/**接口状态 0 失败  1 成功*/
	private int status=0;
	/**接口返回信息*/
	private String msg="失败";
	/**接口数据泛型*/
	private Object data;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	
}
