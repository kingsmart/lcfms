package cn.lcfms.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	
	public enum Formatter{
		SHORT("yyyyMMdd"),_SHORT("yyyy-MM-dd"),LONG("yyyy-MM-dd HH:mm:ss");	
		private String time;
		public String getTime() {
			return this.time;
		}
		Formatter(String pattern) {
			SimpleDateFormat sdf=new SimpleDateFormat(pattern);
			Date date = new Date();
			time = sdf.format(date);
		}
	}
	
	public static String getCurrentDateTime() {
		return Formatter._SHORT.getTime();
	}
	
	public static String getCurrentDateTime(Formatter formatter) {	
		return formatter.getTime();
	}
	
	public static void main(String[] args) {
		StringBuffer buffer=new StringBuffer("{");
		String viewname="{code:1,msg:添加成功,url:index}";
		viewname=viewname.substring(1, viewname.length()-1);
		String[] split = viewname.split(",");
		for(int i=0;i<split.length;i++) {
			String str=split[i];
			int index=str.indexOf(":");
			int length=str.length();
			buffer.append("\""+str.substring(0, index)+"\":\""+str.substring(index+1, length)+"\"");
			if(i!=split.length-1) {
				buffer.append(",");
			}			
		}	
		buffer.append("}");
		System.out.println(buffer.toString());
	}
	
}
