<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
<link rel="stylesheet" href="${CSS}font-awesome.min.css" />
<link rel="stylesheet" href="${CSS}ace.min.css" />
<link rel="stylesheet" href="${CSS}edittag.css" />
<!--[if !IE]> -->
<script src="${JS}jquery-2.1.4.min.js"></script>
<!-- <![endif]-->
<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->
<script src="${JS}edittag.js"></script>
<script src="${JS}bootstrap.min.js"></script>
<script src="${JS}ace-elements.min.js"></script>
<script src="${JS}ace.min.js"></script>
<script src="${JS}showtag.js"></script>
</head>
<body style="background: #fff;">
	<form name="Editform">
		<div class="form-group">
			<label class="control-label" type="text">提示标题：</label>
			<div class="controls">
				<input name="title" value="提示" type="text" style="width: 100%;" />
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">提示内容：</label>
			<div class="controls">
				<textarea name="content" style="width: 100%;">这里是提示的内容，&lt;a href='#' class='alert-link'&gt;超链接&lt;/a&gt;</textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">位置：</label>
			<div class="controls">
				<select class="form-control input-medium" name="position">
					<option value="topRight">右上角</option>
					<option value="bottomRight">右下角</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">手动关闭：</label>
			<div class="controls">
				<select class="form-control input-medium" name="closeBtn">
					<option value="true">是</option>
					<option value="false">否</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">自动关闭：</label>
			<div class="controls">
				<select class="form-control input-medium" name="autoClose">
					<option value="true">是</option>
					<option value="false">否</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">样式：</label>
			<div class="controls">
				<select class="form-control input-medium" name="style">
					<option value="alert-success" selected>绿底success</option>
					<option value="alert-info">淡蓝info</option>
					<option value="alert-warning">黄底warning</option>
					<option value="alert-danger">红底danger</option>
				</select>
			</div>
		</div>
	</form>
	<div class="form-group">
		<label class="control-label" type="text"></label>
		<div class="controls">
			<button class="btn btn-sm btn-primary btn-block"
				onclick="saveEdit();">保存效果</button>
		</div>
	</div>
	<script type="text/javascript">
	function saveEdit(){	
	    var modalElement=getEditHtml();
		var title=document.Editform.title.value;
		var content=document.Editform.content.value;
		var style=document.Editform.style.value;
		var position=document.Editform.position.value;
		var closeBtn=document.Editform.closeBtn.value;
		var autoClose=document.Editform.autoClose.value;
		var str="";
		str+="function showAlert(){";
		str+="$.alert({";
		str+="title:'"+title+"',";
		str+="content:\""+content+"\",";
		str+="style:'"+style+"',";
		str+="position:'"+position+"',";
		str+="closeBtn:"+closeBtn+",";
		str+="autoClose:"+autoClose+"";
		str+="});";
		str+="}";
		getEditWindow().$("body script").html(str);		
		getEditWindow().showAlert=function(){
			getEditWindow().$.alert({
				title:title,
				content:content,
				style:style,
				position:position,
				closeBtn:closeBtn,
				autoClose:autoClose
			});
		}
	}	
</script>
</body>
</html>
