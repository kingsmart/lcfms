<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}form.css"/>
	<link rel="stylesheet" href="${CSS}kalendae.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}form.js"></script>
	<script src="${JS}kalendae.js"></script>	
</head>
<body style="padding-top:30px;">
<div class="container-fluid">
<div class="row">
	<div class="form-box form-color-grey">
		<div class="form-header">
			<h6 class="form-title">
				<i class="fa fa-sort"></i>
				表单标题
			</h6>			
		</div>	
		<form class="form-horizontal" name="myform" method="post" action="">
			<div class="form-body" id="appendHtml">	
				<div class="form-group">
					<label class="col-sm-2 control-label">选择日历：</label>
				    <div class="col-sm-10">
				      <input class="form-control calendar"  name="calendar" type="text" months="1" mode="single" direction="future" readonly/>
				    </div>
				</div>
			</div>	
			<div class="form-footer">
				<div class="form-group">
					<label class="col-sm-2"></label>
					<div class="col-sm-10">
						<button type="button" id="save" class="btn btn-sm btn-grey"><i class="fa fa-check"></i> 提交</button>
						<button type="reset" id="reset" class="btn btn-sm btn-grey"><i class="fa fa-undo"></i> 重置</button>
					</div>					
				</div>
			</div>
		</form>
	</div>				
  </div>
</div> 
<script>
var myform=form.init("myform");	
$("#save").click(function(){
	myform.submit();
});
$("#reset").click(function(){
	myform.reset();
});
</script> 

</body>
</html>     

