<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}spinner.css" />
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}spinner.js"></script>
</head>
<body>
<div class="container-fluid" id="appendHtml">
<div class="row">
	<input type="text" readonly value="1" class="spinner-input" min-spinner="1" max-spinner="10" step-spinner="1">	
</div>
</div>
<script>
$(function(){
	spinner.init();
});
</script>
</body>
</html>      
