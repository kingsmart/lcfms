<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<link rel="stylesheet" href="${CSS}chosen.min.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script type="text/javascript" src="${JS}chosen.jquery.min.js"></script> 
	<script src="${JS}showtag.js"></script>
<style>
.form-group {
	margin-bottom: 5px;
}
</style>
</head>
<body style="background: #fff;">
	<form name="Editform">
		<div class="form-group">
			<label class="control-label" type="text">方向：</label>
			<div class="controls">
				<div class="radio">
					<label> <input name="orientation" type="radio" class="ace" value="horizontal" checked /> <span class="lbl"> 横向</span></label> 
					<label> <input name="orientation" type="radio" class="ace" value="vertical" /> <span class="lbl"> 纵向</span></label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">长度：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="len" value="400" style="width: 80px;" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">样式：</label>
			<div class="controls">
				<select class="input-medium" name="style1">
					<option value="ui-slider-green" selected>绿底</option>
					<option value="ui-slider-red">红底</option>
					<option value="ui-slider-purple">紫底</option>
					<option value="ui-slider-orange">黄底</option>
					<option value="ui-slider-dark">黑底</option>					
				</select>
				<select class="input-medium" name="style2">
					<option value="" selected>方块按钮</option>
					<option value=" ui-slider-small">圆块按钮</option>
					<option value=" ui-slider-simple">透明按钮</option>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">按钮：</label>
			<div class="controls">
				<div class="radio">
					<label> <input name="radios" type="radio" class="ace" value="1" checked /> <span class="lbl"> 一个</span>
					</label> 
					<label> <input name="radios" type="radio" class="ace" value="2" /> <span class="lbl"> 两个</span>
					</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">默认值：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="defaltvalue" value="50"/>
				</div>
				两个滑块时可以设置两个默认值，比如：20,50，用逗号隔开
			</div>		
		</div>
		<div class="form-group">
			<label class="control-label" type="text">提示：</label>
			<div class="controls">
				<div class="radio">
					<label> <input name="tool" type="radio" class="ace" value="1" checked /> <span class="lbl"> 是</span>
					</label> 
					<label> <input name="tool" type="radio" class="ace" value="0" /> <span class="lbl"> 否</span>
					</label>
				</div>
			</div>
		</div>
		<div class="form-group" id="tsgz">
			<label class="control-label" type="text">提示规则：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="toolfix" value="第{i}个" />
				</div>
			</div>
		</div>		
		<div class="form-group">
			<label class="control-label" type="text">最小值：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="min" value="0" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">最大值：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="max" value="100" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">步值：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="step" value="5" />
				</div>
			</div>
		</div>		
		<div class="form-group">
			<label class="control-label" type="text">可滑动到最小值：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="tmin" value="0" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label" type="text">可滑动到最大值：</label>
			<div class="controls">
				<div class="clearfix">
					<input type="text" name="tmax" value="90" />
				</div>
			</div>
		</div>
	</form>
	<div class="form-group">
		<label class="control-label" type="text"></label>
		<div class="controls">
			<button class="btn btn-sm btn-primary btn-block"
				onclick="saveEdit();">保存效果</button>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#simple-colorpicker-1').ace_colorpicker();
			$("input[name=tool]").click(function(){
				var v=$(this).val();
				if(v==0){
					$("#tsgz").hide();
				}else{
					$("#tsgz").show();
				}
			});
			
		});
		function saveEdit() {
			var modalElement = getEditHtml();
			var orientation=document.Editform.orientation.value;
			var len=document.Editform.len.value;
			var style1=document.Editform.style1.value;
			var style2=document.Editform.style2.value;
			var radios=$("input[name=radios]:checked").val();
			var defaltvalue=document.Editform.defaltvalue.value;
			var tool=$("input[name=tool]:checked").val();
			var toolfix=document.Editform.toolfix.value;
			var min=document.Editform.min.value;
			var max=document.Editform.max.value;
			var step=document.Editform.step.value;
			var tmin=document.Editform.tmin.value;
			var tmax=document.Editform.tmax.value;
			var str = "";
			var scr = "";
			str += "<div class=\"slider\" id=\"slider-range\"></div>";
			str += "<script type=\"text/javascript\">";
			str += "function slider(){";
			str += "$('#slider-range').addClass('"+style1+style2+"');";
			if(orientation=='vertical'){
				str += "$('#slider-range').empty().css('height','"+len+"px').slider({";
			}else{
				str += "$('#slider-range').empty().css('width','"+len+"px').slider({";
			}		
			str += "orientation: '"+orientation+"',";
			if(radios==2){
				str += "range: true,";
			}else{
				str += "range: 'min',";	
			}					
			str += "min: "+min+",";
			str += "max: "+max+",";
			str += "step: "+step+",";
			if(radios==2){
				if(defaltvalue.indexOf(',')!=-1){
					str += "values:["+defaltvalue+"],";
				}else{
					str += "values:[0,"+defaltvalue+"],";
				}				
			}else{
				str += "value:"+defaltvalue+",";
			}				
			str += "slide: function( event, ui ) {";
			str += "var val = parseInt(ui.value);";
			str += "if(val<"+tmin+"){";
			str += "return false;";
			str += "}";
			str += "if(val>"+tmax+"){";
			str += "return false;";
			str += "}";
			if(tool==1){
				var l=toolfix.length-1;
				if(l==1){
					l=2;
				}
				var m=0;//提示偏移
				switch(l){
					case 2:
					m=-5;
					break;									
					case 3:
					m=-13;
					break;									
					case 4:
					m=-20;
					break;
					case 5:
					m=-26;
					break;
					case 6:
					m=-34;
					break;
					case 7:
					m=-41;
					break;
					case 8:
					m=-48;
					break;	
					case 9:
					m=-54;
					break;	
					case 10:
					m=-62;
					break;									
				}					
				str += "if(!ui.handle.firstChild) {";
				str += "$(ui.handle).append(\"<div class='tooltip top in' style='display:none;left:"+m+"px;top:-35px;'><div class='tooltip-arrow'></div><div class='tooltip-inner' style='width:"+(l*16)+"px'></div></div>\");";
				str += "}";
				var t=toolfix.replace('{i}','\'+val+\'');	
				str += "$(ui.handle.firstChild).show().children().eq(1).text('"+t+"');";
			}			
			str += "}";
			str += "});";
			if(tool==1){					
				str+="$('#slider-range').children('.ui-slider-handle').blur(function(){";
				str+="$(this).children('.tooltip').hide();";
				str+="});";
			}
			str += "}";
			str += "<\/script>";
			var js_begin="<script type=\"text/javascript\">";    
            var js_end="<\/script>";    
            scr=str.substr(str.indexOf(js_begin)+31);    
            scr=scr.substring(0,scr.indexOf(js_end));  	
            modalElement.children().find(".col-xs-12").html(str);	
			getEditWindow().eval(scr+"slider();");
			
		}
	</script>
</body>
</html>
