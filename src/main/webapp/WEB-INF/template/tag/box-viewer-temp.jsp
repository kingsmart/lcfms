<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}viewer.min.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}viewer.min.js"></script>
	<style>
	   img{cursor:pointer;}
	</style>
</head>
<body style="text-align:center;padding-top:30px;">
<div class="container-fluid">
<div class="row" id="picList">
	<img data-original="${APP}statics/ace/images/avatars/avatar1.png" src="${APP}statics/ace/images/avatars/avatar1.png" alt="图片1"><br/><br/>
	<img data-original="${APP}statics/ace/images/avatars/avatar3.png" src="${APP}statics/ace/images/avatars/avatar3.png" alt="图片3"><br/><br/>
	<img data-original="${APP}statics/ace/images/avatars/avatar4.png" src="${APP}statics/ace/images/avatars/avatar4.png" alt="图片4"><br/><br/>
	<img data-original="${APP}statics/ace/images/avatars/avatar5.png" src="${APP}statics/ace/images/avatars/avatar5.png" alt="图片5"><br/><br/>
	<img data-original="${APP}statics/ace/images/avatars/avatar.png" src="${APP}statics/ace/images/avatars/avatar.png" alt="图片6"><br/><br/>
</div>
</div>
<script>
	var viewer = new Viewer(document.getElementById('picList'), {
		url: 'data-original'
	});
</script>
</body>
</html>
 