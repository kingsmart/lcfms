<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">样式：</label>
	<div class="controls">
		<select class="form-control input-medium" name="style">
			<option value="1">箭头</option>
			<option value="2">加减</option>
			<option value="3">手指</option>		
			<option value="4">娃娃</option>		
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">栏目间距：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="borders" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="borders" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
<div id="tabnumber">
	<div class="form-group tableline">
		<label class="control-label" type="text">第1列：</label>
		<div class="controls">
			列名-<input type="text" placeholder="栏目1" class="linename" style="width:33%" value="栏目1"/> 
		</div>
	</div>
	<div class="form-group tableline">
		<label class="control-label" type="text">第2列：</label>
		<div class="controls">
			列名-<input type="text" placeholder="栏目2" class="linename" style="width:33%" value="栏目2"/>
			<a class="red" href="#" onclick="deleteline(this);">
				<i class="icon-trash bigger-130"></i>
			</a>
		</div>
	</div>
	<div class="form-group tableline">
		<label class="control-label" type="text">第3列：</label>
		<div class="controls">
			列名-<input type="text" placeholder="栏目3" class="linename" style="width:33%" value="栏目3"/>
			<a class="red" href="#" onclick="deleteline(this);">
				<i class="icon-trash bigger-130"></i>
			</a>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		<button class="btn btn-xs btn-success addline"><i class="icon-plus"></i>增加列</button>
	</div>
</div>
<div id="hidetableline" style="display:none;">
	<div class="form-group tableline">
		<label class="control-label" type="text"></label>
		<div class="controls">
			列名-<input type="text" placeholder="新增栏目" class="linename" style="width:33%" value="新增栏目"/>
			<a class="red" href="#" onclick="deleteline(this);">
				<i class="icon-trash bigger-130"></i>
			</a>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">     
	    
	function deleteline(Element){
		$(Element).parent().parent().remove();
	    countline();	
	}
	jQuery(function($) {										
		$(".addline").click(function(){
		   var hidetableline=$("#hidetableline").html();
		   $("#tabnumber").append(hidetableline);
		   countline();	   
		});		
	});
		
	function saveEdit(){
		var modalElement=getEditHtml();
		var linename=$("#tabnumber .tableline .linename");	
		var style=document.Editform.style.value;
		var str="";		
		var tan=randomNumber(1000,9999);	
		$(linename).each(function(i){
			var name=$(this).val();
			var ran=randomNumber(1000,9999);	
			switch(style){
			case '1':
			str+="<div class=\"panel panel-default\"><div class=\"panel-heading\"><a href=\"#acc-"+ran+"\" data-parent=\"#acc-parent-"+tan+"\" data-toggle=\"collapse\" class=\"accordion-toggle collapsed\"><i class=\"fa fa-chevron-right\"></i>&nbsp; "+name+"</a></div><div class=\"panel-collapse collapse\" id=\"acc-"+ran+"\"><div class=\"panel-body\">栏目"+(i+1)+"内容</div></div></div>";
			break;
			case '2':
			str+="<div class=\"panel panel-default\"><div class=\"panel-heading\"><a href=\"#acc-"+ran+"\" data-parent=\"#acc-parent-"+tan+"\" data-toggle=\"collapse\" class=\"accordion-toggle collapsed\"><i class=\"fa fa-plus\"></i>&nbsp; "+name+"</a></div><div class=\"panel-collapse collapse\" id=\"acc-"+ran+"\"><div class=\"panel-body\">栏目"+(i+1)+"内容</div></div></div>";
			break;
			case '3':
			str+="<div class=\"panel panel-default\"><div class=\"panel-heading\"><a href=\"#acc-"+ran+"\" data-parent=\"#acc-parent-"+tan+"\" data-toggle=\"collapse\" class=\"accordion-toggle collapsed\"><i class=\"fa fa-hand-o-right\"></i>&nbsp; "+name+"</a></div><div class=\"panel-collapse collapse\" id=\"acc-"+ran+"\"><div class=\"panel-body\">栏目"+(i+1)+"内容</div></div></div>";
			break;
			case '4':
			str+="<div class=\"panel panel-default\"><div class=\"panel-heading\"><a href=\"#acc-"+ran+"\" data-parent=\"#acc-parent-"+tan+"\" data-toggle=\"collapse\" class=\"accordion-toggle collapsed\"><i class=\"fa fa-frown-o\"></i>&nbsp; "+name+"</a></div><div class=\"panel-collapse collapse\" id=\"acc-"+ran+"\"><div class=\"panel-body\">栏目"+(i+1)+"内容</div></div></div>";
			break;
			}				
		});	
		modalElement.find(".panel-group").html(str);
		var borders=$("input[name=borders]:checked").val();
		if(borders==0){
			modalElement.find(".panel-group>.panel").addClass("nopadding");
		}else{
			modalElement.find(".panel-group.panel").removeClass("nopadding");
		}
		modalElement.find(".panel-group").attr("id","acc-parent-"+tan);
		getEditWindow().collapse.init();
	}

	function countline(){
		var len=$("#tabnumber").find(".tableline");
		for(var i=0;i<len.length;i++){		   
			$(len[i]).find("label").html("第"+(i+1)+"列：");
			$(len[i]).find("input").attr("placeholder","栏目"+(i+1));
			$(len[i]).find("input").attr("value","栏目"+(i+1));		
		}
	}
</script>
</body>
</html>

