<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}tabs.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
<div class="row">
    <div class="col-xs-12" style="padding-top:12px;">
	<div class="tabbable" id="myTab">
		<ul class="nav nav-tabs">
			<li class="active">
				<a data-toggle="tab" href="#home">
					栏目1
				</a>
			</li>
			<li>
				<a data-toggle="tab" href="#profile">
					栏目2
				</a>
			</li>
			<li>
				<a data-toggle="tab"  href="#dropdown">
					栏目3		
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="home" class="tab-pane in active column">
			<p>栏目1内容</p>
			</div>
	
			<div id="profile" class="tab-pane column">
			<p>栏目2内容</p>
			</div>
	
			<div id="dropdown" class="tab-pane column">
			<p>栏目3内容</p>
			</div>
		</div>
	</div>
	</div>
</div>
</div>