<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />	
</head>
<body style="background:#fff;">
	<div class="alert alert-block alert-success">
			<button type="button" class="close" data-dismiss="alert">
				<i class="ace-icon fa fa-times"></i>
			</button>
			<i class="ace-icon fa fa-check green"></i>
			<strong class="green">
				表格使用的是开源插件bPage,想了解更多功能可以 <a target="_blank" href="https://terryz.github.io/bpage/index.html">点击这里</a>
			</strong>
			<p class="danger">但不过选择框跟分页我都做了部分修改,所以最好是使用我这里下载的源码哟！</p>
	</div>
</body>
</html>
