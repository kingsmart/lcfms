<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}widgets.css"/>	
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}widgets.js"></script>
</head>
<body style="padding-top:30px;">
<div class="container-fluid">
   <div class="row">
   <div class="widget-box widget-color-green">
			<div class="widget-header widget-header-small">
			<h6 class="widget-title">
				<i class="fa fa-sort"></i>
				标题内容
			</h6>
			<div class="widget-toolbar">
				<a href="javascript:void(0);" data-action="fullscreen"><i class="fa fa-expand"></i></a>

				<a href="javascript:void(0);" data-action="reload"><i class="fa fa-refresh"></i></a>

				<a href="javascript:void(0);" data-action="collapse"><i class="fa fa-chevron-up"></i></a>

				<a href="javascript:void(0);" data-action="close"><i class="fa fa-minus"></i></a>
			</div>
		</div>

		<div class="widget-body"><div class="widget-body-inner" style="display: block;">
			<div class="widget-main" contenteditable="true">
				<p class="alert alert-info">
					生命诚可贵<br/>
					爱情价更高<br/>
					若要修把各<br/>
					二者皆可抛<br/>
				</p>
			</div>
		</div>
		</div>
	</div>
   </div>
</div>  
<script>
$(function(){
	widget.init();
});
</script>   


	