(function($){
	window.laocheng={
		//保存url路径
		app_path:location.host,	
		//取得cookie    
		 getCookie:function (name) {    
				 var nameEQ = name + "=";    
				 var ca = document.cookie.split(';');    //把cookie分割成组    
				 for(var i=0;i < ca.length;i++) {    
				 var c = ca[i];                      //取得字符串    
				 while (c.charAt(0)==' ') {          //判断一下字符串有没有前导空格    
				 c = c.substring(1,c.length);      //有的话，从第二位开始取    
				 }    
				 if (c.indexOf(nameEQ) == 0) {       //如果含有我们要的name    
				 return unescape(c.substring(nameEQ.length,c.length));    //解码并截取我们要值    
				 }    
				 }    
				 return false;    
				}, 
		//清除cookie    
		clearCookie:function (name) {    
				setCookie(name, "", -1);    
		}, 
		//设置cookie   
		setCookie:function(name, value, seconds) {    
		 seconds = seconds || 0;   //seconds有值就直接赋值，没有为0，这个根php不一样。    
		 var expires = "";    
		 if (seconds != 0 ) {      //设置cookie生存时间    
		 var date = new Date();    
		 date.setTime(date.getTime()+(seconds*1000));    
		 expires = "; expires="+date.toGMTString();    
		 }    
		 document.cookie = name+"="+escape(value)+expires+"; path=/";   //转码并赋值    
		 },		
		//去空格
		trim:function (str){
		var str=str.replace( /^\s*/, "");
		var str=str.replace( /\s*$/, "");
		return str;
		},
		//获取指定数之间的随机数
		random:function (min,max){
		return Math.floor(min+Math.random()*(max-min));
		},
		//保存一个随机数
		randomNumber:0,		
		//绑定fixed的DIV
		fix:function(id){
			function htmlScroll()  
			{  
			    var top = document.body.scrollTop ||  document.documentElement.scrollTop;  
			    if(elFix.data_top < top)  
			    {  
			        elFix.style.position = 'fixed';  
			        elFix.style.top = 0;  
			        elFix.style.left = elFix.data_left;  
			    }  
			    else 
			    {  
			        elFix.style.position = 'static';  
			    }  
			}  			 
			function htmlPosition(obj)  
			{  
			    var o = obj;  
			    var t = o.offsetTop;  
			    var l = o.offsetLeft;  
			    while(o = o.offsetParent)  
			    {  
			        t += o.offsetTop;  
			        l += o.offsetLeft;  
			    }  
			    obj.data_top = t;  
			    obj.data_left = l;  
			}  			 
			var oldHtmlWidth = document.documentElement.offsetWidth;  
			window.onresize = function(){  
			    var newHtmlWidth = document.documentElement.offsetWidth;  
			    if(oldHtmlWidth == newHtmlWidth)  
			    {  
			        return;  
			    }  
			    oldHtmlWidth = newHtmlWidth;  
			    elFix.style.position = 'static';  
			    htmlPosition(elFix);  
			    htmlScroll();  
			}  
			window.onscroll = htmlScroll;   
			var elFix = document.getElementById(id);  
			htmlPosition(elFix);  
		}
	}
	$(function(){
		//激活提示框
		$('[data-rel=tooltip]').tooltip();
		$('[data-rel=tooltip]').tooltip({
			show: null,
			position: {
				my: "left top",
				at: "left bottom"
			},
			open: function( event, ui ) {
				ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
			}
		});	
		//动态加载图片
		try{
			var colorbox_params = {
				reposition:true,
				scalePhotos:true,
				scrolling:false,
				previous:'<i class="icon-arrow-left"></i>',
				next:'<i class="icon-arrow-right"></i>',
				close:'&times;',
				current:'{current} of {total}',
				maxWidth:'100%',
				maxHeight:'100%',
				onOpen:function(){
					document.body.style.overflow = 'hidden';
				},
				onClosed:function(){
					document.body.style.overflow = 'auto';
				},
				onComplete:function(){
					$.colorbox.resize();
				}
			};
			$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
			$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");
		}catch(e){
			return;
		}			
	})
})($);

